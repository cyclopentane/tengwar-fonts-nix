{ stdenvNoCC
, fetchzip
, name
, url
, sha256
, stripRoot ? true
}:

stdenvNoCC.mkDerivation {
  inherit name;

  src = fetchzip {
    inherit url sha256 stripRoot;
  };

  phases = [ "unpackPhase" "installPhase" ];

  installPhase = ''
    mkdir -p $out/share/fonts/truetype
    cp $src/*.ttf $out/share/fonts/truetype
  '';
}

