{
  description = "Tengwar fonts, packaged for Nix";

  outputs = { self, ... }: {
    overlays.default = (final: prev: {
      tengwar-formal = final.callPackage ./package.nix {
        name = "tengwar-formal";
        url = "mirror://sourceforge/project/freetengwar/TengwarFont/TengwarFormalCSUR11.zip";
        sha256 = "sha256-vEEnwmH9B6EguATLpdGcm4xlh/9s9MNNuJVHSlodBew=";
      };

      tengwar-telcontar = final.callPackage ./package.nix {
        name = "tengwar-telcontar";
        url = "mirror://sourceforge/project/freetengwar/TengwarFont/TengwarTelcontar.008.zip";
        sha256 = "sha256-EdoBNcaR6uabKe/q8+tfcx3Oj4r29kZjJ5TUlN0jtds=";
        stripRoot = false;
      };

      free-mono-tengwar = final.callPackage ./package.nix {
        name = "free-mono-tengwar";
        url = "mirror://sourceforge/project/freetengwar/TengwarFont/FreeMonoTengwar.2013-07-21.zip";
        sha256 = "sha256-14DAaP7frfT/EaaZ11DrNSnSVBJjRnS63IbO8R8pnL4=";
      };
    });

    nixosModules.default = { pkgs, ... }: {
      nixpkgs.overlays = [ self.overlays.default ];
      fonts.fonts = with pkgs; [
        tengwar-telcontar
        tengwar-formal
        free-mono-tengwar
      ];
    };
  };
}
